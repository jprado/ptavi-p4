#!/usr/bin/python3
# Jorge Prado Fernandez
"""
Programa cliente UDP que abre un socket a un servidor
"""

import socket
import sys


if sys.argv[3] != 'register':
    sys.exit("""Usage: python3 client.py ip puerto register
                sip_address expires_value (xxxx)""")

# Constantes. Dirección IP del servidor y contenido a enviar
SERVER = sys.argv[1]
PORT = int(sys.argv[2])
USER = sys.argv[4]
EXPIRES = sys.argv[5]
LINE = str("REGISTER sip:" + USER + " SIP/2.0" + "\r\n" +
           "Expires: " + EXPIRES + "\r\n\r\n")

# Creamos el socket, lo configuramos y lo atamos a un servidor/puerto
with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:

    my_socket.connect((SERVER, PORT))
    print("Enviando:", str(LINE))
    my_socket.send(bytes(str(LINE), 'utf-8') + b'\r\n')
    data = my_socket.recv(1024)  # Tamaño del buffer
    print('Recibido -- ', data.decode('utf-8'))

print("Socket terminado.")
