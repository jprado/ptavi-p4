#!/usr/bin/python3
# Jorge Prado Fernandez
"""
Clase (y programa principal) para un servidor de eco en UDP simple
"""

import json
import socketserver
import sys
import time


class SIPRegisterHandler(socketserver.DatagramRequestHandler):
    """
    Echo server class
    """
    dic = {}

    def json2registered(self):
        """
        Buscamos el archivo json en nuestra carpeta, si existe, usamos los
        datos contenidos como nuestro diccionario actual, si no existe pasamos
        """
        try:
            with open("registered.json") as fich:
                self.dic = json.load(fich)
        except FileNotFoundError:
            pass

    def deluser(self):
        """
        Bucle para eliminar usuarios del diccionario
        """
        lista = []
        actual_time = (time.time())

        for user in self.dic:
            if int(actual_time) >= int(self.dic[user]['expires']):
                lista.append(user)
        print("Usuarios que se eliminan")
        print(lista)
        print("")
        for user in lista:
            del self.dic[user]

    def register2json(self):
        """
        Bloque para crear archivos json
        """
        with open("registered.json", "w") as fich:
            json.dump(self.dic, fich, indent=4)

    def handle(self):
        """
        handle method of the server class
        (all requests will be handled by this method)
        """
        self.json2registered()
        line = self.rfile.read()
        print('')
        petition = line.decode('utf-8')
        print("El cliente nos manda ", line.decode('utf-8'))
        phrase = petition.split()
        phrase2 = phrase[1].split(':')
        user = phrase2[-1]
        ip = self.client_address[0]
        expires = int(phrase[4])
        expired = int(expires) + int(time.time())
        """
        Procedimiento para registrar usuarios, primero borramos los antiguos
        """
        self.deluser()

        if phrase[0] != "REGISTER":
            print("ERROR 404: este servidor solo acepta REGISTER SIP")
        if expires == 0:
            if user in self.dic:
                del self.dic[user]
            self.wfile.write(b"SIP/2.0 200 OK\r\n\r\n")
        else:
            self.dic[user] = {'address': ip, 'expires': expired}

        self.register2json()
        """
        Imprimimos el diccionario y el SIP
        """
        print("El diccionario es el siguiente: ")
        print(self.dic)
        print("")
        self.wfile.write(b"SIP/2.0 200 OK\r\n\r\n")


if __name__ == "__main__":
    # Listens at localhost ('') and in the port that we write in the shell
    # and calls the EchoHandler class to manage the request

    puerto = int(sys.argv[1])
    serv = socketserver.UDPServer(('', puerto), SIPRegisterHandler)

    print("Lanzando servidor UDP de eco...")
    try:
        serv.serve_forever()
    except KeyboardInterrupt:
        print("Finalizado servidor")
